import React, {Component} from 'react';
import Number from './components/Number/Number';

import './App.css';

let finalNumbers;

const compareNumbers = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
};

const createNumbers = () => {
    let numbers = [];
    while (numbers.length < 5) {
        let ifTrue = true;
        const random = Math.floor(Math.random() * 32) + 5;
        for (let key in numbers) {
            if (numbers[key] === random) {
                ifTrue = false;
            }
        }
        if (ifTrue === true) {
            numbers.push(random);
        }
    }
    numbers.sort(compareNumbers);
    finalNumbers = numbers;
};

createNumbers();

class App extends Component {
    state = {
        numbs: finalNumbers
    };

    changeNumbers = () => {
        createNumbers();
        let thisNumbers = [...this.state.numbs];
        for (let i = 0; i < thisNumbers.length; i++) {
            thisNumbers[i] = finalNumbers[i];
        }
        this.setState({numbs: thisNumbers});
    };

    render() {
        return (
            <div className="numbers-container">
                <button onClick={this.changeNumbers} id="newNumbers">New numbers</button>
                <div className="numbers">
                    <Number number={this.state.numbs[0]}/>
                    <Number number={this.state.numbs[1]}/>
                    <Number number={this.state.numbs[2]}/>
                    <Number number={this.state.numbs[3]}/>
                    <Number number={this.state.numbs[4]}/>
                </div>
            </div>
        );
    }
}

export default App;
